/*jslint vars: true, forin: true, sloppy: true */
/*global angular, probe, URLs*/

probe.run(function ($httpBackend) {
	//let oc-dialog pass through
	$httpBackend.whenGET(/\.html$/).passThrough();
	//查询会话状态
	$httpBackend.whenGET(URLs.APIOriginUrl + "user/session").respond({
		"user": { //用户部分信息
			"name": 'lyy' //用户名
		},
		"limitation": { //限额情况
			"caseNumber": 5, //case限额
			"dictionaryNumber": 134, //字典限额
			"objectNumber": 13, //元件限额
			"processNumber": 13, //子过程限额
			"teamNumber": 123, //子过程限额
			"positionNumber": 155, //子过程限额
			"roleNumber": 1 //子过程限额
		},
		"team": { //项目所属团队 可以为null
			"name": 'ddd' //项目所属团队名称
		},
		"position": { //激活探针的角色的职位 可以为null
			"name": 'ddddd', //职位昵称
			"privilege": 223334 //权限代码
		}
	});
	//刷新会话
	$httpBackend.whenGET(URLs.APIOriginUrl + "user/noop").respond({});
	//登录
	$httpBackend.whenPOST(URLs.APIOriginUrl + "user/login").respond({});
	//登出
	$httpBackend.whenGET(URLs.APIOriginUrl + "user/logout").respond({});
	//查询我的所有职位
	$httpBackend.whenGET(URLs.APIOriginUrl + "user/position/list").respond({
		data: {
			"positions": [
				{
					"positionId": 1, //职位ID
					"name": 'position1', //职位名称
					"comment": 'nubili', //职位概述
					"teamName": '团队名称1' //团队名称
				},
				{
					"positionId": 2, //职位ID
					"name": 'position1', //职位名称
					"comment": 'nubili', //职位概述
					"teamName": '团队名称2' //团队名称
				}
			]
		},
		state: 200
	});
	//选择“我”的工作身份
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "user/position/" + "\\d+$")).respond({});
	//退出“我”的工作身份
	$httpBackend.whenGET(URLs.APIOriginUrl + "user/position/exit/").respond({});
	//注册用户
	$httpBackend.whenPOST(URLs.APIOriginUrl + "user/").respond({});
	//检查重名
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "user/" + "\\d+" + "/exist$")).respond({});
	//修改密码
	$httpBackend.whenPUT(URLs.APIOriginUrl + "user/password/").respond({});
	//查询我的用户信息
	$httpBackend.whenGET(URLs.APIOriginUrl + "user/").respond({
		data: {
			"user": {
				"userId": 111, //用户ID
				"name": 'niubi', //用户名
				"email": 'hehe@qq.com', //邮箱
				"company": 'bilibili', //公司
				"job": 'web', //工作
				"birthday": '2015-11-1', //生日
				"createtime": 20151101, //注册时间
				"updatetime": 20151104 //最近修改时间
			}
		},
		state: 200
	});
	//修改我的用户信息
	$httpBackend.whenPUT(URLs.APIOriginUrl + "user/").respond({});
	//查询“我”创建的团队的列表
	$httpBackend.whenGET(URLs.APIOriginUrl + "user/team/list").respond({
		data: {
			"teams": [
				{
					"teamId": 1, //团队id
					"name": '团队名1', //团队名
					"updatetime": 20151104 //最近修改时间
				},
				{
					"teamId": 2, //团队id
					"name": '团队名2', //团队名
					"updatetime": 20151104 //最近修改时间
				},
				{
					"teamId": 3, //团队id
					"name": '团队名3', //团队名
					"updatetime": 20151104 //最近修改时间
				},
				{
					"teamId": 4, //团队id
					"name": '团队名4', //团队名
					"updatetime": 20151104 //最近修改时间
				}
			]
		}
	});
	//查询当前工作角色待测项目列表
	$httpBackend.whenGET(URLs.APIOriginUrl + "user/project/list").respond({
		data: {
			"projects": [
				{
					"projectId": 1, //项目ID
					"name": 'projectName', //项目名称
					"updatetime": 20151104 //最近修改时间
				},
				{
					"projectId": 3, //项目ID
					"name": 'projectName', //项目名称
					"updatetime": 20151104 //最近修改时间
				},
				{
					"projectId": 4, //项目ID
					"name": 'projectName', //项目名称
					"updatetime": 20151104 //最近修改时间
				},
				{
					"projectId": 5, //项目ID
					"name": 'projectName', //项目名称
					"updatetime": 20151104 //最近修改时间
				},
				{
					"projectId": 7, //项目ID
					"name": 'projectName', //项目名称
					"updatetime": 20151104 //最近修改时间
				}
			]
		},
		state: 200
	});
	//创建一个待测项目的令牌
	$httpBackend.whenPOST(URLs.APIOriginUrl + "token/").respond({});
	//删除一个待测项目的令牌
	$httpBackend.whenDELETE(new RegExp("^" + URLs.APIOriginUrl + "token/" + "[a-f0-9]{16}$")).respond({});
	//根据项目ID与Session要素查询一个测项目的令牌
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "token/project/" + "\\d+$")).respond({
		data: {
			"token": {
				"tokenId": 192, //令牌ID
				"tokenCode": 919182, //令牌代码
				"expiredtime": '201-11-5' //过期时间
			}
		},
		state: 200
	});
	//修改一个待测项目
	$httpBackend.whenPUT(new RegExp("^" + URLs.APIOriginUrl + "project/" + "\\d+$")).respond({});
	//删除一个待测项目
	$httpBackend.whenDELETE(new RegExp("^" + URLs.APIOriginUrl + "project/" + "\\d+$")).respond({});
	//查询一个待测项目
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "project/" + "\\d+$")).respond({
		data: {
			"project": {
				"projectId": 1, //项目id
				"name": 'projectName', //项目名称
				"comment": '描述', //项目描述
				"createtime": 20151105, //创建时间
				"updatetime": 20151108 //最近修改时间
			}
		},
		state: 200
	});
	//查询某个待测项目的仿真测试用例列表
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "project/" + "\\d+" + "/case/list$")).respond({
		data: {
			"cases": [
				{
					"caseId": 1, //用例的ID
					"name": '用例名称', //用例的名称
					"updatetime": 20151105 //最近修改时间
				},
				{
					"caseId": 2, //用例的ID
					"name": '用例名称', //用例的名称
					"updatetime": 20151105 //最近修改时间
				}
			]
		},
		state: 200
	});
	//查询某个待测项目的字典列表
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "project/" + "\\d+" + "/dictionary/list$")).respond({
		data: {
			"dictionaries": [
				{
					"dictionaryId": 1,
					"name": '字典01',
					"updatetime": 20151105 //最近修改时间
				},
				{
					"dictionaryId": 2,
					"name": '字典02',
					"updatetime": 20151105 //最近修改时间
				},
				{
					"dictionaryId": 3,
					"name": '字典03',
					"updatetime": 20151105 //最近修改时间
				}
			]
		}
	});
	//查询某个待测项目的公共元素列表
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "project/" + "\\d+" + "/object/list$")).respond({
		data: {
			"objects": [
				{
					"objectId": 1, //用例的ID
					"name": 'objectName01', //用例的名称
					"cssPath": 'div>p>a', //css选择器路径
					"createtime": 20151105, //创建时间
					"updatetime": 20151108 //最近修改时间
				},
				{
					"objectId": 2, //用例的ID
					"name": 'objectName02', //用例的名称
					"cssPath": 'div>p>a', //css选择器路径
					"createtime": 20151105, //创建时间
					"updatetime": 20151108 //最近修改时间
				},
				{
					"objectId": 3, //用例的ID
					"name": 'objectName03', //用例的名称
					"cssPath": 'div>p>a', //css选择器路径
					"createtime": 20151105, //创建时间
					"updatetime": 20151108 //最近修改时间
				}
			]
		},
		state: 200
	});
	//查询某个待测项目的公共子过程
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "project/" + "\\d+" + "/process/list$")).respond({
		data: {
			"process": [
				{
					"processId": 1, //子过程的ID
					"name": 'processName', //子过程名称
					"updatetime": 20151105 //最近修改时间
				},
				{
					"processId": 2, //子过程的ID
					"name": 'processName', //子过程名称
					"updatetime": 20151105 //最近修改时间
				},
				{
					"processId": 3, //子过程的ID
					"name": 'processName', //子过程名称
					"updatetime": 20151105 //最近修改时间
				}
			]
		},
		state: 200
	});
	//查询某个待测项目的测试报告
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "project/" + "\\d+" + "/result/list$")).respond({
		data: {
			"results": [
				{
					"resultId": 1, //报告的ID
					"name": '报告01', //报告名称
					"updatetime": 20151105 //最近修改时间
				},
				{
					"resultId": 2, //报告的ID
					"name": '报告02', //报告名称
					"updatetime": 20151105 //最近修改时间
				}
			]
		},
		state: 200
	});
	//创建一个团队
	$httpBackend.whenPOST(URLs.APIOriginUrl + "team/").respond({});
	//修改一个团队
	$httpBackend.whenPUT(new RegExp("^" + URLs.APIOriginUrl + "team/" + "\\d+$")).respond({});
	//删除一个团队
	$httpBackend.whenDELETE(new RegExp("^" + URLs.APIOriginUrl + "team/" + "\\d+$")).respond({});
	//查询一个团队
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "team/" + "\\d+$")).respond({
		team: {
			"teamId": 1, //团队ID
			"name": 'teamName01', //团队名
			"comment": '注释', //团队注释
			"createtime": 20151104, //创建时间
			"updatetime": 20151105 //最近修改时间
		}
	});
	//查询某个团队的职位列表
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "team/" + "\\d+" + "/position/list$")).respond({
		data: {
			"positions": [
				{
					"positionId": 1, //职位ID
					"name": '职位名称01', //职位名称
					"userId": 123, //用户ID
					"updatetime": 20151105 //最近修改时间
				}
			]
		},
		state: 200
	});
	//查询某个团队的角色列表
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "team/" + "\\d+" + "/role/list$")).respond({
		data: {
			"roles": [
				{
					"rolesId": 1, //职位ID
					"name": '组长', //职位名称
					"updatetime": 20151105 //最近修改时间
				}
			]
		}
	});
	//查询某个团队的待测项目列表
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "team/" + "\\d+" + "/project/list$")).respond({
		data: {
			"projects": [
				{
					"projectId": 1, //项目ID
					"name": '项目名称', //项目名称
					"updatetime": 20151105 //最近修改时间
				}
			]
		},
		state: 200
	});
	//创建一个职位
	$httpBackend.whenPOST(URLs.APIOriginUrl + "position/").respond({});
	//修改一个职位
	$httpBackend.whenPUT(new RegExp("^" + URLs.APIOriginUrl + "position/" + "\\d+$")).respond({});
	//修改一个职位
	$httpBackend.whenDELETE(new RegExp("^" + URLs.APIOriginUrl + "position/" + "\\d+$")).respond({});
	//查询一个职位
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "position/" + "\\d+$")).respond({
		data: {
			"position": {
				"positionId": 1, //职位ID
				"name": '职位名01', //职位名称
				"comment": '注释', //职位注释
				"privilege": 1, //职位特权
				"createtime": 20151104, //创建时间
				"updatetime": 20151105 //最近修改时间
			}
		},
		state: 200
	});
	//查询某个职位的角色列表
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "position/" + "\\d+" + "/role/list$")).respond({
		data: {
			"projects": [
				{
					"projectId": 1, //项目ID
					"name": '角色1', //项目名称
					"updatetime": 20151105 //最近修改时间
				},
				{
					"projectId": 2, //项目ID
					"name": '角色2', //项目名称
					"updatetime": 20151105 //最近修改时间
				},
				{
					"projectId": 3, //项目ID
					"name": '角色3', //项目名称
					"updatetime": 20151105 //最近修改时间
				}
			]
		},
		state: 200
	});

	//查询某个职位的用户（这个模型还没设计）
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "position/" + "\\d+" + "/user$")).respond({
		data: {
			"user": {
				"userId": 1, //用户ID
				"name": 'username01', //用户名
				"email": 'jdeiji@qq.com' //用户名
			}
		},
		state: 200
	});
	/*-----------------任命/解雇/路径-----------------------*/
	//任命/解雇一个用户
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "position/" + "\\d+" + "/user" + "\\d+$")).respond({});

	//创建一个角色
	$httpBackend.whenPOST(URLs.APIOriginUrl + "role/").respond({});
	//修改一个角色
	$httpBackend.whenPUT(new RegExp("^" + URLs.APIOriginUrl + "role/" + "\\d+$")).respond({});
	//删除一个角色
	$httpBackend.whenDELETE(new RegExp("^" + URLs.APIOriginUrl + "role/" + "\\d+$")).respond({});
	//查询一个角色
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "role/" + "\\d+$")).respond({
		data: {
			"role": {
				"roleId": 1, //角色ID
				"name": 'roleName01', //角色名称
				"comment": '注释', //角色注释
				"privilege": 2 //角色特权
			}
		},
		state: 200
	});
	//查询一个仿真测试用例
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "case/" + "\\d+$")).respond({
		data: {
			"case": {
				"caseId": 1, //用例的ID
				"name": 'caseName01', //用例的名称
				"code": 'casecodehehehe', //用例程序代码
				"comment": 'miaoshu', //测试用例描述
				"createtime": 20151103, //创建时间
				"updatetime": 20151108, //最近修改时间
				"dictionary": 2 //关联字典的ID 可能不设置字典
			}
		},
		state: 200
	});
	// 删除一个仿真测试用例
	$httpBackend.whenDELETE(new RegExp("^" + URLs.APIOriginUrl + "case/" + "\\d+$")).respond({});
	// 查询一个公共元件
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "object/" + "\\d+$")).respond({});
	// 删除一个公共元件
	$httpBackend.whenDELETE(new RegExp("^" + URLs.APIOriginUrl + "object/" + "\\d+$")).respond({});
	// 查询一个公共子过程
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "process/" + "\\d+$")).respond({
		data: {
			"process": {
				"processId": 1, //子过程的ID
				"name": 'processName01', //子过程名称
				"code": '过程代码', //子过程代码
				"comment": '描述', //子过程描述
				"createtime": 20151104, //创建时间
				"updatetime": 20151106 //最近修改时间
			}
		},
		state: 200
	});
	// 删除一个公共子过程
	$httpBackend.whenDELETE(new RegExp("^" + URLs.APIOriginUrl + "process/" + "\\d+$")).respond({});
	// 查询一个数据字典
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "dictionary/" + "\\d+$")).respond({
		data: {
			"dictionary": {
				"dictionaryId": 1,
				"name": '字典01',
				"comment": '注释',
				"field": 'string(JSON)',
				"assignment": 'string(JSON)',
				"createtime": 20151105, //创建时间
				"updatetime": 20151108 //最近修改时间
			}
		},
		state: 200
	});
	// 删除一个数据字典
	$httpBackend.whenDELETE(new RegExp("^" + URLs.APIOriginUrl + "dictionary/" + "\\d+$")).respond({});
	// 查询一个测试报告
	$httpBackend.whenGET(new RegExp("^" + URLs.APIOriginUrl + "result/" + "\\d+$")).respond({
		data: {
			"result": {
				"resultId": 1,
				"name": '报告名称',
				"comment": '注释',
				"data": 'string(JSON)', //结果数据
				"createtime": 20151105, //创建时间
				"updatetime": 20151107 //最近修改时间
			}
		},
		state: 200
	});
	// 删除一个测试报告
	$httpBackend.whenDELETE(new RegExp("^" + URLs.APIOriginUrl + "result/" + "\\d+$")).respond({});
});