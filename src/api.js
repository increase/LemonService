/*jslint vars: true, forin: true, sloppy: true */
/*global angular, probe, URLs, console */
probe.run(function (ocAPI, $http, positionPanels) {
	ocAPI.setAPIOrigin(URLs.serviceDataRemoteAPIOrigin);
	window.$ = ocAPI;
	var errors = {
		'1': 'You are not signed in.',
		'3': 'This request expect a correct captcha.',
		'6': 'You do not have permission to operate.',
		'400': 'Username or password was wrong.',
		'411': 'No position can be used.'
	};
	// http://192.168.31.160:8080/probe/stateCode

	ocAPI.interceptor('accessException', function (response) {
		var state = response.state;
		if (state === 6) {
			console.warn(errors[state]);
			throw state;
		}
	});
	ocAPI.interceptor('unsignedException', function (response) {
		var state = response.state;
		if (state === 1) {
			throw state;
		}
	});
	ocAPI.interceptor('captchaException', function (response) {
		var state = response.state;

		if (state === 3) {
			throw state;
		}
	});
	ocAPI.interceptor('userException', function (response) {
		var state = response.state;

		if (state >= 400 && state < 500) {
			throw new Error(errors[state]);
		}
	});
	ocAPI.interceptor('InactiveException', function (response) {
		var state = response.state;

		if (state === 419) {
			throw response;
		}
	});

	(function () {
		/**
		 * Module: User
		 * Action:
		 *		create
		 *		update
		 *		updatePassword
		 *		retrieval
		 *		noop
		 *		login
		 *		logout
		 *		listMyProject
		 *		listMyPosition
		 *		listMyPosition
		 *		checkUserName
		 */
		ocAPI.API('createUser', { //OK SUCCESS
			request: function (userBody, captcha) {
				return $http.post(this.APIOrigin + "user/", userBody, {
					params: {
						captcha: captcha
					}
				});
			},
			interceptor: ['captchaException']
		});
		ocAPI.API('updateUser', { //READY
			request: function (userId, userBody) {
				return $http.put(this.APIOrigin + "user/" + userId, userBody);
			}
		});
		ocAPI.API('updatePassword', { //READY
			request: function (passwordBody, captcha) {
				return $http.put(this.APIOrigin + "user/password", passwordBody, {
					params: {
						capctha: captcha
					}
				});
			}
		});
		ocAPI.API('getMyInfo', { //READY
			request: function () {
				return $http.get(this.APIOrigin + "user");
			},
			cacheKey: 'MY_INFO'
		});
		ocAPI.API('noop', { //OK SUCCESS
			request: function () {
				return $http.get(this.APIOrigin + "user/noop");
			},
			interceptor: ['unsignedException']
		});
		ocAPI.API('login', { //OK SUCCESS
			request: function (loginBody, captcha) {
				return $http.post(this.APIOrigin + "user/login", loginBody, {
					params: {
						captcha: captcha
					}
				});
			},
			interceptor: ['captchaException', 'InactiveException', 'userException']
		});
		ocAPI.API('logout', { //OK SUCCESS
			request: function () {
				this.cache.removeAll();
				window.location.reload();
				return $http.get(this.APIOrigin + "user/logout");
			}
		});
		ocAPI.API('listMyProject', { //OK
			request: function () {
				return $http.get(this.APIOrigin + 'user/project/list');
			},
			cacheKey: 'MY_PROJECT_LIST'
		});
		ocAPI.API('listMyPosition', { //OK
			request: function () {
				return $http.get(this.APIOrigin + "user/position/list");
			},
			cacheKey: 'MY_POSITION_LIST'
		});
		ocAPI.API('listMyTeam', { //OK
			request: function () {
				return $http.get(this.APIOrigin + "user/team/list");
			},
			cacheKey: 'TEAM_LIST',
			interceptor: ['unsignedException']
		});
		ocAPI.API('checkValueExisted', { //OK
			request: function (value, type) {
				return $http.get(this.APIOrigin + "user/" + type + '/' + value + "/existence");
			}
		});
		ocAPI.API('top10', { //OK
			request: function (username) {
				return $http.get(this.APIOrigin + "user/top10/" + username);
			}
		});
		ocAPI.API('resendActiveCodeWithNewEmail', {
			request: function (userBody) {
				return $http.post(this.APIOrigin + 'user/activation', userBody);
			}
		});
	}()); // User 11, OK 8, READY 3
	(function () {
		/**
		 * Module: Project
		 * Action:
		 *		create
		 *		delete
		 *		update
		 *		retrieval
		 *		createToken
		 *		deleteToken
		 *		getToken
		 *		listCaseOfProject
		 *		listDictionaryOfProject
		 *		listResultOfProject
		 *		getObject
		 *		updateObject
		 */
		ocAPI.API('createProject', { //OK SUCCESS
			request: function (projectBody, teamId, positionId) {
				return $http.post(this.APIOrigin + "project/", projectBody, {
					params: {
						team: teamId,
						position: positionId
					}
				});
			},
			interceptor: ['unsignedException', 'accessException']
		});
		ocAPI.API('deleteProject', { //OK SUCCESS
			request: function (projectId, positionId, teamId) {
				this.cache.remove('PROJECT_' + projectId);
				return $http['delete'](this.APIOrigin + "project/" + projectId, {
					params: {
						position: positionId,
						team: teamId
					}
				});
			},
			interceptor: ['unsignedException', 'accessException']
		});
		ocAPI.API('updateProject', { //OK SUCCESS
			request: function (projectId, projectBody, positionId, teamId) {
				this.cache.remove('PROJECT_' + projectId);
				return $http.put(this.APIOrigin + "project/" + projectId, projectBody, {
					params: {
						position: positionId,
						team: teamId
					}
				});
			},
			interceptor: ['unsignedException', 'accessException']
		});
		ocAPI.API('getProject', { //OK SUCCESS
			request: function (projectId, teamId, positionId) {
				return $http.get(this.APIOrigin + "project/" + projectId, {
					params: {
						position: positionId,
						team: teamId
					}
				});
			},
			cacheKey: 'PROJECT_'
		});
		ocAPI.API('createToken', { //OK FIX
			request: function (projectId, tokenBody) {
				return $http.post(this.APIOrigin + "project/" + projectId + "/token", tokenBody);
			}
		});
		ocAPI.API('deleteToken', { //OK SUCCESS
			request: function (tokenCode, projectId) {
				return $http['delete'](this.APIOrigin + "project/" +
									   projectId + "/token/" + tokenCode);
			}
		});
		ocAPI.API('getTokenLike', { //OK SUCCESS
			request: function (projectId, positionId) {
				return $http.get(this.APIOrigin + "project/" + projectId + '/token', {
					params: {
						asPosition: positionId
					}
				});
			}
		});
		ocAPI.API('getObject', { //OK SUCCESS
			request: function (projectId) {
				return $http.get(this.APIOrigin + "project/" + projectId + "/objects");
			},
			cacheKey: 'OBJECT_OF_PROJECT_'
		});
		ocAPI.API('listProjectTeam', { //OK
			request: function (teamId, positionId) {
				return $http.get(this.APIOrigin + "project/list", {
					params: {
						team: teamId,
						position: positionId
					}
				});
			},
			cacheKey: 'PROJECT_LIST_OF_TEAM_',
			interceptor: ['unsignedException', 'accessException']
		});
	}()); // Project 12, OK 12
	(function () {
		/**
		 * Module: Team
		 * Action:
		 *		create
		 *		delete
		 *		update
		 *		retrieval
		 *		listPositionOfTeam
		 *		listRoleOfTeam
		 *		listProjectTeam
		 */
		ocAPI.API('createTeam', { //OK SUCCESS
			request: function (teamBody) {
				this.cache.remove('TEAM_LIST');
				return $http.post(this.APIOrigin + "team", teamBody);
			},
			interceptor: ['unsignedException', 'accessException']
		});
		ocAPI.API('deleteTeam', { //OK SUCCESS
			request: function (teamId, captcha) {
				this.cache.remove('TEAM_LIST');
				this.cache.remove('TEAM_' + teamId);
				return $http['delete'](this.APIOrigin + "team/" + teamId, {
					params: {
						captcha: captcha
					}
				});
			},
			interceptor: ['captchaException', 'unsignedException', 'accessException']
		});
		ocAPI.API('updateTeam', { //OK SUCCESS
			request: function (teamId, teamBody) {
				this.cache.remove('TEAM_LIST');
				return $http.put(this.APIOrigin + "team/" + teamId, teamBody);
			},
			interceptor: ['unsignedException', 'accessException']
		});
		ocAPI.API('getTeam', { //OK SUCCESS
			request: function (teamId) {
				return $http.get(this.APIOrigin + "team/" + teamId);
			},
			cacheKey: 'TEAM_',
			interceptor: ['unsignedException']
		});
	}()); // Team 7, OK 7
	(function () {
		/**
		 * Module: Position
		 * Action:
		 *		create
		 *		delete
		 *		update
		 *		retrieval
		 *		listRoleOfPosition
		 *		getUserOfPosition
		 *		assignUser
		 */
		ocAPI.API('createPosition', { //OK SUCCESS
			request: function (positionBody, teamId) {
				return $http.post(this.APIOrigin + "position", positionBody, {
					params: {
						team: teamId
					}
				});
			}
		});
		ocAPI.API('updatePosition', { //OK SUCCESS
			request: function (positionId, positionBody, teamId) {
				this.cache.remove('ROLE_LIST_OF_POSITION_' + positionId);
				return $http.put(this.APIOrigin + "position/" + positionId, positionBody, {
					params: {
						team: teamId
					}
				});
			}
		});
		ocAPI.API('deletePosition', { //OK SUCCESS
			request: function (positionId, teamId) {
				return $http['delete'](this.APIOrigin + "position/" + positionId, {
					params: {
						team: teamId
					}
				});
			}
		});
		ocAPI.API('getPosition', { //OK SUCCESS
			request: function (positionId, teamId) {
				return $http.get(this.APIOrigin + "position/" + positionId, {
					params: {
						team: teamId
					}
				});
			}
		});
		ocAPI.API('listPositionOfTeam', { //OK SUCCESS
			request: function (teamId) {
				return $http.get(this.APIOrigin + "position/list", {
					params: {
						team: teamId
					}
				});
			},
			cacheKey: 'POSITION_LIST_OF_TEAM_'
		});
		ocAPI.API('getUserOfPosition', {
			request: function (positionId) { //OK SUCCESS
				return $http.get(this.APIOrigin + "position/" + positionId + "/user");
			},
			cacheKey: 'USER_OF_POSITION_'
		});
		ocAPI.API('assignUser', { //OK SUCCESS
			request: function (positionId, userId, action) {
				return $http.put(this.APIOrigin + "position/" + positionId + "/user/" + userId, null, {
					params: {
						assign: action
					}
				});
			}
		});
	}()); // Position 7, OK 7
	(function () {
		/**
		 * Module: Role
		 * Action:
		 *		create
		 *		delete
		 *		update
		 *		retrieval
		 */
		ocAPI.API('createRole', { //OK SUCCESS
			request: function (roleBody, teamId) {
				return $http.post(this.APIOrigin + "role/", roleBody, {
					params: {
						team: teamId
					}
				});
			}
		});
		ocAPI.API('deleteRole', { //OK SUCCESS
			request: function (roleId, teamId) {
				return $http['delete'](this.APIOrigin + "role/" + roleId, {
					params: {
						team: teamId
					}
				});
			}
		});
		ocAPI.API('updateRole', { //OK SUCCESS
			request: function (roleId, teamId, roleBody) {
				return $http.put(this.APIOrigin + "role/" + roleId, roleBody, {
					params: {
						team: teamId
					}
				});
			}
		});
		ocAPI.API('getRole', { //OK SUCCESS
			request: function (roleId, teamId) {
				return $http.get(this.APIOrigin + "role/" + roleId, {
					params: {
						team: teamId
					}
				});
			}
		});
		ocAPI.API('listRoleOfTeam', { //OK SUCCESS
			request: function (teamId) {
				return $http.get(this.APIOrigin + "role/list", {
					params: {
						team: teamId
					}
				});
			},
			cacheKey: 'ROLE_LIST_OF_TEAM_'
		});
		ocAPI.API('listRoleOfPosition', { //OK
			request: function (positionId, teamId) {
				return $http.get(this.APIOrigin + "role/list", {
					params: {
						position: positionId,
						team: teamId
					}
				});
			},
			cacheKey: 'ROLE_LIST_OF_POSITION_'
		});
	}()); // Role 4, OK 4
	(function () {
		/**
		 * Module: Case
		 * Action:
		 *		delete
		 *		retrieval
		 */
		ocAPI.API('getCase', {
			request: function (caseId, projectId) { //ERROR
				return $http.get(this.APIOrigin + "case/" + caseId, {
					params: {
						project: projectId
					}
				});
			},
			cacheKey: 'CASE_',
			interceptor: ['unsignedException', 'accessException']
		});
		ocAPI.API('deleteCase', {
			request: function (caseId, projectId) { //OK
				return $http['delete'](this.APIOrigin + "case/" + caseId, {
					params: {
						project: projectId
					}
				});
			},
			interceptor: ['unsignedException', 'accessException']
		});
		ocAPI.API('listCaseOfProject', { //OK SUCCESS
			request: function (projectId) {
				return $http.get(this.APIOrigin + "case/list", {
					params: {
						project: projectId
					}
				});
			},
			cacheKey: 'CASE_LIST_OF_PROJECT_',
			interceptor: ['unsignedException', 'accessException']
		});
	}()); // Case 2, OK 2
	(function () {
		/**
		 * Module: Process
		 * Action:
		 *		delete
		 *		retrieval
		 */
		ocAPI.API('getProcess', {
			request: function (processId) { //ERROR
				return $http.get(this.APIOrigin + "process/" + processId);
			},
			cacheKey: 'PROCESS_'
		});
		ocAPI.API('deleteProcess', {
			request: function (processId) { //ERROR
				return $http['delete'](this.APIOrigin + "process/" + processId);
			}
		});
		ocAPI.API('listProcessOfProject', {
			request: function (projectId) {
				return $http.get(this.APIOrigin + "project/" + projectId + "/process/list");
			},
			cacheKey: 'PROCESS_LIST_OF_PROJECT_'
		});
	}()); // Process 2
	(function () {
		/**
		 * Module: Dictionary
		 * Action:
		 *		delete
		 *		retrieval
		 */
		ocAPI.API('getDictionary', { //OK
			request: function (dictionaryId, projectId) {
				return $http.get(this.APIOrigin + "dictionary/" + dictionaryId, {
					params: {
						project: projectId
					}
				});
			},
			cacheKey: 'DICTIONARY_',
			interceptor: ['unsignedException', 'accessException']
		});
		ocAPI.API('deleteDictionary', {
			request: function (dictionaryId, projectId) { //OK
				return $http['delete'](this.APIOrigin + "dictionary/" + dictionaryId, {
					params: {
						project: projectId
					}
				});
			},
			interceptor: ['unsignedException', 'accessException']
		});
		ocAPI.API('listDictionaryOfProject', { //OK SUCCESS
			request: function (projectId) {
				return $http.get(this.APIOrigin + "dictionary/list", {
					params: {
						project: projectId
					}
				});
			},
			cacheKey: 'DICTIONARY_LIST_OF_PROJECT_',
			interceptor: ['unsignedException', 'accessException']
		});
	}()); // Dictionary 2, OK 2
	(function () {
		/**
		 * Module: Result
		 * Action:
		 *		delete
		 *		retrieval
		 */
		ocAPI.API('getResult', {
			request: function (resultId, projectId) { //OK
				return $http.get(this.APIOrigin + "result/" + resultId, {
					params: {
						project: projectId
					}
				});
			},
			cacheKey: 'RESULT_',
			interceptor: ['unsignedException', 'accessException']
		});
		ocAPI.API('deleteResult', {
			request: function (resultId, projectId) { //OK
				return $http['delete'](this.APIOrigin + "result/" + resultId, {
					params: {
						project: projectId
					}
				});
			},
			interceptor: ['unsignedException', 'accessException']
		});
		ocAPI.API('listResultOfProject', { //OK SUCCESS
			request: function (projectId) {
				return $http.get(this.APIOrigin + "result/list", {
					params: {
						project: projectId
					}
				});
			},
			cacheKey: 'RESULT_LIST_OF_PROJECT_',
			interceptor: ['unsignedException', 'accessException']
		});
	}()); // Result 3, OK 3

});
