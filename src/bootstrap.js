/*jslint vars: true, forin: true, sloppy: true */
/*global angular, probe, URLs */
probe.run(function (ocAPI, $http, $interval, ocDialog, $rootScope, wikiAPI, userAccount) {
	$interval(function () {
		ocAPI.use('noop').by();
	}, 240000);

	ocAPI.use('noop').by().then(function (data) {
		ocDialog.getNewDI('user/account').then(function (DI) {
			DI.openDialogFrom();
		});
	});
	ocAPI.getNow();

	$rootScope.probeSampleUrl = URLs.probeSampleUrl;
	$rootScope.resourcesUrl = URLs.resourcesUrl;

	$rootScope.user = userAccount;
});
