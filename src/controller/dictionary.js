/*jslint vars: true, forin: true, sloppy: true */
/*global probe, angular */
probe.controller("Dictionary", function ($scope, $element, $window, $http, ocDialog) {
  $scope.trashDictionary = {
		title: '警告 - 全部清空',
		message: '您确定要清空整个项目吗？'
	};
	$scope.field  = (function () {
		var j, len = 15, d = [];
		for (j = 0; j < len; j++) {
			d.push(
				{
					id: j,
					name: '字典' + j + '号',
					value: '注释哇哈哈哈哈哈哈啊哈'
				}
			);
		}
		return d;
	}());
	//字段列表
	$scope.fieldList = {
		header: [
			{
				key: 'id',
				text: 'ID',
				width: 50
			},
			{
				key: 'name',
				text: '字段名称',
				width: 140
			},
			{
				key: 'value',
				text: '字段值',
				width: 240
			}
		]
	};
});
