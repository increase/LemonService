/*jslint vars: true, forin: true, sloppy: true */
/*global probe, angular */
probe.controller('Global', function ($scope, ocDialog) {
    var openLogin = function () {
        ocDialog.getNewDI('user/login').then(function (DI) {
            DI.openDialogFrom().$$scope
                .closeButtonFn = function () {
                    $scope.isLogoShow = true;
                };
        });
    };

    $scope.isLogoShow = false;
    $scope.showLogin = function () {
		$scope.isLogoShow = false;
        openLogin();
	};

    openLogin();
});
