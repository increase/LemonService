/*jslint vars: true, forin: true, sloppy: true */
/*global probe, angular, URLs */
probe.controller('Main', function ($scope, ocDialog, $element, ocAPI,
									positionPanels, userAccount) {
	var isMyProjectOpened = false,
		DI = ocDialog.queryDI($element);

	DI.$$scope.title = 'Lemonce - 测试管理 v' + URLs.version;

	$scope.homePanel = {
		type: 'account',
		name: '账户'
	};

	$scope.myProjectPanel = {
		type: 'myProject',
		name: '我的项目',
		closeFn: function () {
			isMyProjectOpened = false;
		}
	};

	$scope.panels = [];
	$scope.currentPanel = {};

	$scope.switchHome = function () {
		$scope.currentPanel = $scope.homePanel;
	};
	$scope.switchPanel = function (panel) {
		$scope.currentPanel = panel;
	};
	$scope.closeMyProjects = function (event, index) {
		event.stopPropagation();
		($scope.panels.splice(index, 1)[0].closeFn || angular.noop)();
		$scope.switchHome();

	};

	$scope.openMyProject = function () {
		if (isMyProjectOpened) {
			$scope.switchPanel($scope.myProjectPanel);
			return;
		}

		isMyProjectOpened = true;
		$scope.currentPanel = $scope.myProjectPanel;
		$scope.panels.push($scope.myProjectPanel);
	};
	positionPanels.getCurrentPanelShareData =
		$scope.getCurrentPanelShareData = function () {
			return $scope.currentPanel.data;
		};
	$scope.registerNewPanel = function (panel) {
		$scope.panels.push(panel);
	};

	//init
	$scope.switchHome();
	ocAPI.use('getMyInfo').by().then(function (data) {
		console.log(data);
	});
});
