/*jslint sloppy: true */
/*global probe, angular */
probe.controller('Manual', function ($scope, $sce, wikiAPI) {

	$scope.pageList = [];
	wikiAPI.getPageListByCategory('manual').success(function (data) {
		$scope.pageList = data.query.categorymembers;
	});

	$scope.page = $sce.trustAsHtml('正在载入');
	function getPage(pageName) {
		wikiAPI.getPageText(pageName).success(function (page) {
			$scope.page = $sce.trustAsHtml(page.parse.text['*']);
		});
	}
	$scope.viewPage = function (pageName) {
		$scope.page = $sce.trustAsHtml('正在载入');
		getPage(pageName);
	};

	//init
	getPage('首页');
});
