/*jslint sloppy: true */
/*global probe, angular */
probe.controller('Terms', function ($scope, $sce, wikiAPI, ocDialog, $element) {
	var DI = ocDialog.queryDI($element),
		textType = DI.share;

	DI.$$scope.title = {
		terms: '服务条款',
		privacy: '隐私政策',
		disclaimer: '免责声明'
	}[textType];

	$scope.terms = $sce.trustAsHtml('正在加载');
	wikiAPI.getPageText({
		terms: 'Lemonce:关于',
		privacy: 'Lemonce:隐私政策',
		disclaimer: 'Lemonce:免责声明'
	}[textType]).success(function (page) {
		$scope.terms = $sce.trustAsHtml(page.parse.text['*']);
	});
}).controller('Notice', function ($scope, $sce, wikiAPI, ocDialog, $element) {
	var DI = ocDialog.queryDI($element),
		notice = DI.share;

	DI.$$scope.title = notice.title + ' - Lemonce 公告';
	$scope.heading = notice;
	$scope.content = $sce.trustAsHtml('正在加载');
	wikiAPI.getPageText(notice.title).success(function (page) {
		$scope.content = $sce.trustAsHtml(page.parse.text['*']);
	});
});
