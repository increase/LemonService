/*jslint vars: true, forin: true, sloppy: true */
/*global probe, angular */
probe.controller("Appoint", function ($scope, $element, $window, ocAPI, ocDialog) {
	var DI = ocDialog.queryDI($element),
		refreshPositionListOfTeam = DI.share.refreshPositionListOfTeam,
		positionId = DI.share.positionId;

    $scope.assigned = {
		title: '指派 - 职位',
		message: '您确定要将用户指派到该职位？',
	    callFn: function () {
		    var userId = $scope.user[$scope.userList.selected].userId;
		    ocAPI.use('assignUser').by(positionId, userId, true)
			    .then(function () {
			        DI.closeDialog();
				    refreshPositionListOfTeam();
	            });
	    }
	};
	$scope.user = [];
	//用户列表
	$scope.findUser = '';
	$scope.search = function () {
		if ($scope.findUser.trim() === '') { return; }

		ocAPI.use('top10').by($scope.findUser)
			.then(function (data) {
				$scope.user = data.users;
				refreshList();
			});
	};

	$scope.userList = {
		selected: -1,
		header: [
			{
				key: 'avatar',
				text: '头像',
				width: 50
			},
			{
				key: 'username',
				text: '用户名',
				width: 200
			}
		]
	};
	$scope.refresh = true;
	function refreshList () {
		$scope.refresh = !$scope.refresh;
	}
});
