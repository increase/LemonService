/*jslint vars: true, forin:true, sloppy: true*/
/*global angular, probe*/
probe.controller('configProbe', function ($scope, $element, ocDialog, ocAPI) {
	var DI = ocDialog.queryDI($element),
		project = DI.share.project;

	$scope.probe = {
		path: project.probePath,
		origin: project.probeOrigin,
		id: project.probeId
	};

	$scope.pathR = DI.share.path_r;
	$scope.originR = DI.share.origin_r;

	function updateProject() {
		ocAPI.use('updateProject').by(project.projectId, {
			name: project.name,
			probePath: $scope.probe.path,
			probeOrigin: $scope.probe.origin
		}).then(function () {
			project.probePath = $scope.probe.path;
			project.probeOrigin = $scope.probe.origin;
		});
	}

	$scope.submit = function () {
		updateProject();
		DI.closeDialog();
	};
	$scope.apply = function () {
		updateProject();
	};
});

