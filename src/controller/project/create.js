/*jslint vars: true, forin: true, sloppy: true */
/*global probe, angular */
probe.controller('CreateProject', function ($scope, $element, ocDialog, ocAPI) {
	var DI = ocDialog.queryDI($element),
		refreshProjectList = DI.share.refreshProjectList,
		p = DI.share.position || {};

	$scope.project = {
		name: '',
		comment: ''
	};

	$scope.createProject = function () {
		ocAPI.use('createProject').by($scope.project, p.teamId, p.positionId)
			.then(function (data) {
				refreshProjectList();
				DI.closeDialog();
			});
	};
});
