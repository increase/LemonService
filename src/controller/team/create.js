/*jslint vars: true, forin: true, sloppy: true */
/*global probe, angular */
probe.controller('CreateTeam', function ($scope, $element, ocDialog, ocAPI) {
	var createTeamDI = ocDialog.queryDI($element);

	$scope.team = {
		name: '',
		comment: ''
	};

	$scope.createTeam = function () {
		ocAPI.use('createTeam').by($scope.team).then(function (data) {
			createTeamDI.closeDialog();
			createTeamDI.share.refreshTeamsFn();
		});
	};
});
