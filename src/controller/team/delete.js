/*jslint vars: true, forin: true, sloppy: true */
/*global probe, angular */
probe.controller('DeleteTeam', function ($scope, $element, ocDialog, ocAPI) {
	var DI = ocDialog.queryDI($element);

	$scope.captcha = '';

	$scope.deleteTeam = function () {
		ocAPI.use('deleteTeam').by(DI.share.teamId, $scope.captcha).then(function (data) {
			DI.share.refreshTeams();
			DI.closeDialog();
		}, function (error) {
			$scope.error = error.message;
		});
	};
});
