/*jslint vars: true, forin: true, sloppy: true */
/*global probe, angular */
probe.controller("teamEdit", function ($scope, $element, $filter, ocAPI, ocDialog) {
	var teamEditor = ocDialog.queryDI($element),
		teamId = teamEditor.share.teamId,
		dateFilter = $filter('date');

	function updatetimeFilter(list, exp) {
		var newList = [];
		angular.forEach(list, function (obj) {
			var newObj = angular.copy(obj);
			newObj.updatetime = dateFilter(obj.updatetime, exp);
			this.push(newObj);
		}, newList);

		return newList;
	}

	/**
	 * Method about team object
	 */
	$scope.isTeamInfoChanged = false;
	function getTeam() {
		ocAPI.use('getTeam').by(teamId).then(function (data) {
			$scope.team = data.team;
			teamEditor.$$scope.title = data.team.name + ' - 我的团队';
		});
	}
	$scope.refreshTeam = function () {
		ocAPI.cache.remove("TEAM_" + teamId);
		getTeam();
	};
	$scope.updateTeamInfo = function () {
		ocAPI.use('updateTeam').by($scope.team.teamId, $scope.team)
			.then(function (data) {
				teamEditor.share.refreshTeamsFn();
				$scope.refreshTeam();
				$scope.isTeamInfoChanged = false;
			});
	};
	$scope.deleteTeam = function () {
		teamEditor.share.deleteTeamFn($scope.team.teamId, teamEditor.$$parent);
		teamEditor.closeDialog();
	};
	$scope.setChanged = function () {
		$scope.isTeamInfoChanged = true;
	};

	/**
	 * Method about roles.
	 */
	$scope.role = [];
	function getSelectedRoleId() {
		return $scope.role[$scope.roleList.selected].roleId;
	}
	function getRoleListOfTeam() {
		ocAPI.use('listRoleOfTeam').by(teamId).then(function (data) {
			$scope.role = updatetimeFilter(data.roles, 'yyyy-MM-dd');
		});
	}
	$scope.refreshRoleListOfTeam = function () {
		ocAPI.cache.remove('ROLE_LIST_OF_TEAM_' + teamId);
		getRoleListOfTeam();
	};
	$scope.createRole = {
		name: 'role/edit',
		share: function () {
			return {
				teamId: teamId,
				refreshRoleListOfTeam: $scope.refreshRoleListOfTeam
			};
		}
	};
	function deleteRole() {
		ocAPI.use('deleteRole').by(getSelectedRoleId(), teamId).then(function () {
			//TODO 清空position缓存
			$scope.refreshRoleListOfTeam();
		});
	}
	$scope.deleteRole = {
		title: '删除角色 - 警告',
		message: '删除角色会影响到相关职位，是否继续？',
		callFn: deleteRole
	};
	$scope.roleList = {
		selected: -1,
		header: [
			{
				key: 'name',
				text: '角色名称',
				width: 100
			},
			{
				key: 'updatetime',
				text: '更新时间',
				width: 90
			}
		],
        visitFn: function () {
            ocDialog.getNewDI('role/edit', {
				teamId: teamId,
				roleId: getSelectedRoleId(),
				refreshRoleListOfTeam: $scope.refreshRoleListOfTeam
			}).then(function (DI) {
                DI.openDialogFrom(teamEditor);
            });
        }
	};

	/**
	 * Method about positions.
	 */
	$scope.isPositionChanged = false;
	$scope.position = [];
	function getSelectedPostionId() {
		return $scope.position[$scope.positionList.selected].positionId;
	}
	function getPositionListOfTeam() {
		ocAPI.use('listPositionOfTeam').by(teamId).then(function (data) {
			$scope.position = updatetimeFilter(data.positions, 'yyyy-MM-dd');
			$scope.isPositionChanged = !$scope.isPositionChanged;
		});
	}
	$scope.refreshPositionListOfTeam = function () {
		ocAPI.cache.remove('POSITION_LIST_OF_TEAM_' + teamId);
		getPositionListOfTeam();
	};
	$scope.createPosition = {
		name: 'position/edit',
		share: function () {
			return {
				teamId: teamId,
				refreshRoleListOfTeam: $scope.refreshPositionListOfTeam,
				rolesOfTeam: $scope.role
			};
		}
	};
	function deletePosition() {
		ocAPI.use('deletePosition').by(getSelectedPostionId(), teamId)
			.then(function () {
				$scope.refreshPositionListOfTeam();
				$scope.positionList.selected = -1;
			});
	}
	$scope.deletePosition = {
		title: '删除职位 - 警告',
		message: '删除职位将导致该职位的任命一并失效，是否继续？',
		callFn: deletePosition
	};
	$scope.appointPosition = {
		name: 'user/appoint',
		share: function () {
			return {
				positionId: getSelectedPostionId(),
				refreshPositionListOfTeam: $scope.refreshPositionListOfTeam,
			};
		}
	};
	$scope.editPosition = function() {
		ocDialog.getNewDI('position/edit', {
			teamId: teamId,
			positionId: getSelectedPostionId(),
			refreshRoleListOfTeam: $scope.refreshPositionListOfTeam,
			rolesOfTeam: $scope.role
		}).then(function (DI) {
			DI.openDialogFrom(teamEditor);
		});
	};
	$scope.dismiss = function () {
		var userId = $scope.position[$scope.positionList.selected].userId;

		if (!userId) { return; }

		ocDialog.alert({
			title: '取消指派 - 职位',
			message: '您确定要取消指派到该职位的用户吗？',
			callFn: function () {
				ocAPI.use('assignUser').by(getSelectedPostionId(), userId, false)
					.then(function () {
						$scope.refreshPositionListOfTeam();
					});
			}
		}, teamEditor);
	};
	$scope.positionList = {
		selected: -1,
		header: [
			{
				key: 'name',
				text: '职位名称',
				width: 120
			},
			{
				key: 'username',
				text: '用户名称',
				width: 100
			},
			{
				key: 'updatetime',
				text: '更新时间',
				width: 90
			}
		],
		visitFn: function () {
			$scope.editPosition();
		}
	};

	// init.
	getTeam();
	getPositionListOfTeam();
	getRoleListOfTeam();
});
