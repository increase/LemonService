/*jslint vars: true, forin: true, sloppy: true */
/*global probe, angular */
probe.controller("TestLog", function ($scope, $element, $filter, $timeout, ocAPI, ocDialog) {
	var DI = ocDialog.queryDI($element);
	var share = DI.share;

	$scope.tabs = {};
	$scope.stepsList = {
		header: [
			{
				key: 'time',
				text: '时间',
				width: 120
			}, {
				key: 'text',
				text: '事件',
				width: 360
			}
		]
	};

	//time line sorting and filtering
	$scope.transformer = {
		sortType: 'time',
		sortReverse: false,
		searchText: ''
	};

	$scope.clear = function () {
		$scope.sort = {
			type: 'time',
			reverse: false
		};
	};
	$scope.all = function () {
		$scope.timeline = angular.copy($scope.result.flow);
	};

	$scope.$watchCollection('transformer', function () {
		if (!$scope.result) { return; }

		$scope.timeline = $filter('orderBy')($filter('filter')($scope.result.flow, {
			time:'', text:$scope.transformer.searchText
		}), 'transformer.sortType', $scope.transformer.sortReverse);
	});

	$scope.chartOption = {
		hide: [],
		width: 660,
		height: 300
	};

	$scope.toggleDisplay = function (key) {
		var keyPosition = $scope.chartOption.hide.indexOf(key);

		if (keyPosition < 0) {
			$scope.chartOption.hide.push(key);
		} else {
			$scope.chartOption.hide.splice(keyPosition, 1);
		}
	};

	ocAPI.use('getResult').by(share).then(function (data) {
		$scope.result = $filter('result')(data.result);
		$scope.all();//display everything at start
		//set the dialog title
		DI.$$scope.title = $scope.result.name + ' - 测试报告';
		//for chart
		$scope.chartData = $scope.result.chartData;
		//disable if no data
		$scope.tabs.$tabs[2].isDisabled = Object.keys($scope.chartData).length ? false : true;
	});
});
