/*jslint vars: true, forin: true, sloppy: true */
/*global probe, angular */
probe.controller("Account", function ($scope, $element, positionPanels,
									   fullScreen, ocAPI, ocDialog, wikiAPI) {
	var accountDI = ocDialog.queryDI($element);
	accountDI.$$scope.closeButtonFn = function () {
		$scope.logout();
	};

	$scope.menu = [
		{
			title: '账户',
			menu: [
				{
					title: '账户信息',
					attr: 'oc-dialog-open="{name:\'user/info\'}"'
				},
				null,
				{
					title: '退出',
					attr: 'oc-dialog-close ng-click="logout()"'
				}
			]
		},
		{
			title: '帮助',
			menu: [
				{
					title: '帮助文档',
					attr: 'oc-dialog-open="{name:\'other/help\'}"'
				},
				{
					title: '关于',
					attr: 'oc-dialog-open="{name:\'other/about\'}"'
				}
			]
		}
	];

	$scope.positions = [];
	function deletePositionPanel(positionId) {
		delete positionPanels[positionId];
	}
	$scope.openPositionPanel = function (index) {
		var p = $scope.positions[index],
			$pps = positionPanels;

		if (!$pps[p.positionId]) {
			$pps[p.positionId] = {
				type: 'projectInTeam',
				name: '职位：' + p.name,
				data: p,
				closeFn: function () {
					deletePositionPanel(p.positionId);
				}
			};
			$scope.$parent.registerNewPanel($pps[p.positionId]);
		}
		$scope.$parent.switchPanel($pps[p.positionId]);
	};
	function getMyPositionList() {
		ocAPI.use('listMyPosition').by().then(function (data) {
			$scope.positions = data.positions;
		});
	}
	$scope.logout = function () {
		ocAPI.use('logout').by().then(function () {
			ocAPI.getNow();
		});
	};

	$scope.teams = [];
	function getMyTeamList() {
		ocAPI.use('listMyTeam').by().then(function (data) {
			$scope.teams = data.teams;
		});
	}

	$scope.refreshTeams = function () {
		ocAPI.cache.remove('TEAM_LIST');
		getMyTeamList();
	};

	function deleteTeamHandle(teamId, parentDI) {
		ocDialog.getNewDI('team/delete', {
			teamId: teamId,
			refreshTeams: $scope.refreshTeams
		}).then(function (DI) {
			DI.openDialogFrom(parentDI);
		});
	}
	$scope.alertDelete = function (index, event) {
		event.stopPropagation();
		deleteTeamHandle($scope.teams[index].teamId, accountDI);
	};

	$scope.teamCreate = {
		name: 'team/create',
		share: function () {
			return {
				refreshTeamsFn: getMyTeamList
			};
		}
	};

	$scope.openTeam = function (index) {
		ocDialog.getNewDI('team/edit', {
			teamId: $scope.teams[index].teamId,
			deleteTeamFn: deleteTeamHandle,
			refreshTeamsFn: getMyTeamList
		}).then(function (DI) {
			DI.openDialogFrom(accountDI);
		});
	};

	$scope.refreshMyPositions = function () {
		ocAPI.cache.remove('MY_POSITION_LIST');
		getMyPositionList();
	};

	$scope.fullScreen = function () {
		fullScreen();
	};

	$scope.notice = [];
	function getNotice() {
		wikiAPI.getPageListByCategory('notice').success(function (data) {
			var n = [];
			angular.forEach(data.query.categorymembers, function (notice) {
				this.push({
					title: notice.title,
					time: notice.timestamp.substr(0, 10)
				});
			}, n);

			$scope.notice = n;
		});
	}
	$scope.openNotice = function (notice) {
		ocDialog.getNewDI('other/notice', notice).then(function (DI) {
			DI.openDialogFrom(accountDI);
		});
	};


	//init
	getMyPositionList();
	getMyTeamList();
	getNotice();
});
