/*jslint vars: true, forin: true, sloppy: true */
/*global probe, angular */
probe.controller('ActiveAccount', function ($scope, $element, ocDialog, ocAPI, $interval) {
	var user = ocDialog.queryDI($element).share;

	$scope.user = {
		username: user.username,
		password: user.password,
		email: user.email
	};

	$scope.sec = 0;
	$scope.isWaiting = false;
	function waiting60Sec() {
		var end = Date.now() + 60000;
		$scope.isWaiting = true;
		$interval(function () {
			$scope.sec = parseInt((end - Date.now()) / 1000, 0);
			if ($scope.sec <= 1) {
				$scope.isWaiting = false;
			}
		}, 500, 121);
	}
	$scope.activeAccount = function () {
		ocAPI.use('resendActiveCodeWithNewEmail').by($scope.user).then(function () {
			waiting60Sec();
		});
	};

});
