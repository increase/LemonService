/*jslint sloppy: true */
/*global probe, angular */
probe.controller('Register', function ($scope, $element, ocDialog, ocAPI) {
	var $captcha = '', registerDI = ocDialog.queryDI($element);

	$scope.user = {
		username: '',
		password: '',
		email: '',
		confirm: ''
	};

	$scope.register = function () {
		ocAPI.use('createUser').by($scope.user, $scope.captcha()).then(function (data) {
			registerDI.closeDialog();
			ocAPI.getNow();
		}, function () {
			$scope.captchaInfo = true;
		});
	};

	$scope.checkExisted = function (fieldName) {
		ocAPI.use('checkValueExisted').by($scope.user[fieldName], fieldName)
			.then(function (data) {
				$scope['check' + fieldName] = data.isExisted;
			});
	};
	$scope.change = function (fieldName) {
		$scope['check' + fieldName] = false;
	};

	$scope.captcha = function (newValue) {
		if (angular.isDefined(newValue)) {
			$captcha = newValue.substr(0, 4);
		}

		return $captcha;
	};

	$scope.setTextType = function (type) {
		return function () {
			return type;
		};
	};
});
