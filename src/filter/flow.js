/*jslint vars: true, forin:true, sloppy: true*/
/*global angular, probe*/
probe.filter("flow", function ($filter, template) {
	return function (input) {
		var out = [],
			lastPosition = 0,//[0, last position], [0, current position]
			obj = angular.copy(input),
			base = obj.baseTime,
			marks = obj.marks,
			logs = obj.logs,
			timeTemplate = "[${time}]",
			instructionTemplates = {
				"13": "进入 ${arg[2]} 进程",
				"0": "等待 ${arg[2]} 毫秒",
				"1": "${arg[3]} 动作被执行，${arg[4] ? '参数：' + arg[4] + '，': ''}对象 ${arg[2]}",
				"2": "断言开始",
				"-1": "退出第 ${arg[1] + 1} 次测试",
				"100": "发生错误"
			};

		angular.forEach(marks, function (mark) {
			var myLog;
			//ignore [100, xxx] for now
			if (mark[0] === 0) {
				myLog = logs.slice(lastPosition, mark[1]);
				lastPosition = mark[1];

				angular.forEach(myLog, function (log) {
					out.push({
						time: template(timeTemplate, {
							time: $filter("date")(base + log[1], "HH:mm:ss.sss")
						}),
						text: template(instructionTemplates[log[0][0]], {
							arg: log[0]
						})
					});
				});
			}
		});

		return out;
	};
});