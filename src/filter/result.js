/*jslint vars: true, forin:true, sloppy: true*/
/*global probe, angular*/
probe.filter('result', function ($filter) {
	return function (input) {
		var out = angular.copy(input), data = angular.fromJson(input.data), raw;
		delete out.data;

		out.isRelative = out.url && out.url.match(/^[^hfl/]/);

		out = angular.extend(out, data);
		delete out.rawData;

		try {
			raw = angular.fromJson(data.rawData);
			out.flow = $filter('flow')(raw);
			out.chartData = raw.performance;
			delete out.chartData.length;
		} catch (e) {
			out.flow = [];//suppress errors from corrupted data
			out.name += '(数据被破坏？)';
		}

		return out;
	}
});