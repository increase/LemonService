/*jslint vars: true, sloppy: true, evil: true */
/*global angular, URLs */
URLs.dialogRoutePath = "./src/dialog/";
URLs.probeSample = 'http://www.or-change.com/probe2/probe.html';

if (window.location.href !== URLs.serviceEndUrl) {
	window.location.href = URLs.serviceEndUrl;
}

var probe = angular.module("probe", [
	'oc-dialog',
	'ngMessages',
//	'ngMockE2E',
	'oc-APIs'
]).config(function (ocDialogProvider, $sceProvider, $httpProvider) {
	ocDialogProvider.config("ROUTE_PATH", URLs.dialogRoutePath || '');
	ocDialogProvider.config("BASE_Z_INDEX", 500);

	$httpProvider.defaults.withCredentials = true;

}).constant("template", function (string, scope) {
	return string.replace(/\$\{([\s\S]*?)\}/g, function ($, code) {
		var scoped = code.replace(/(["'\.\w\$]+)/g, function (match) {
			return (/["'\d]/).test(match[0]) ? match : 'scope.' + match;
		});
		try {
			return new Function('scope', 'return ' + scoped)(scope);
		} catch (e) { return '错误：\n' + e; }
	});
}).constant("positionPanels", {
}).constant('userAccount', {
}).constant('privilege', {
	'1': {"module": "测试项目", "action": "查询"},
	'2': {"module": "测试项目", "action": "创建"},
	'3': {"module": "测试项目", "action": "更改"},
	'4': {"module": "测试项目", "action": "删除"},
	'101': {"module": "测试用例", "action": "查询"},
	'102': {"module": "测试用例", "action": "创建"},
	'103': {"module": "测试用例", "action": "更改"},
	'104': {"module": "测试用例", "action": "删除"},
	'201': {"module": "数据字典", "action": "查询"},
	'202': {"module": "数据字典", "action": "创建"},
	'203': {"module": "数据字典", "action": "更改"},
	'204': {"module": "数据字典", "action": "删除"},
	'301': {"module": "测试报告", "action": "查询"},
	'302': {"module": "测试报告", "action": "创建"},
	'303': {"module": "测试报告", "action": "删除"}
});
