/*jslint vars: true, forin: true, sloppy: true */
/*global probe, angular */
probe.service('fullScreen', function () {
	function launchFullscreen(element) {
		if (element.requestFullscreen) {
			element.requestFullscreen();
		} else if (element.mozRequestFullScreen) {
			element.mozRequestFullScreen();
		} else if (element.webkitRequestFullscreen) {
			element.webkitRequestFullscreen();
		} else if (element.msRequestFullscreen) {
			element.msRequestFullscreen();
		}
	}

	return function () {
		launchFullscreen(document.documentElement);
	};
});
