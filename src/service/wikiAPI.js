/*jslint vars: true, forin: true, sloppy: true */
/*global probe, angular, URLs */
probe.service('wikiAPI', function ($http, $cacheFactory) {
	var queryParams, wikiAPI, wikiCache, myOrigin;

	wikiCache = $cacheFactory('wiki');
	myOrigin = window.location.origin;

	wikiAPI = URLs.wikiOrigin + 'api.php';
	queryParams = {
		pageText: {
			'action': 'parse',
			'page': null,
			'prop': 'text|links|sections',
			'format': 'json',
			'disabletoc': 1,
			'disableeditsection': 1,
			'disablepp': 1,
			'origin': myOrigin
		},
		categoryMembers: {
			'action': 'query',
			'list': 'categorymembers',
			'cmtitle': null,
			'utf8': 1,
			'format': 'json',
			'cmprop': 'title|timestamp',
			'origin': myOrigin
		}
	};

	function getPageText(pageName) {
		var o = queryParams.pageText;
		o.page = pageName;

		return $http.get(wikiAPI, {
			cache: wikiCache,
			params: o
		});
	}

	function getPageListByCategory(categoryName) {
		var o = queryParams.categoryMembers;
		o.cmtitle = 'category:' + categoryName;

		return $http.get(wikiAPI, {
			cache: wikiCache,
			params: o
		});
	}

	return {
		getPageText: getPageText,
		getPageListByCategory: getPageListByCategory,
		cache: wikiCache
	};
});
